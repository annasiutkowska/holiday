package com.annasiutkowska.holiday.controllers;


import com.annasiutkowska.holiday.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

@RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<User> requestTest(){
    return new ResponseEntity<User>(new User("login","password"), HttpStatus.OK);
}
}

